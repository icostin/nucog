; ******************************************************************************
; 
; nucog - header-only library for working with type-safe numeric constant groups.
; 
; Written in 2019 by Costin Ionescu <costin.ionescu@gmail.com>
; 
; 
; To the extent possible under law, the author has dedicated all copyright 
; and related and neighboring rights to this software to the public domain 
; worldwide. 
; This software is distributed without any warranty.
; 
; You should have received a copy of the CC0 Public Domain Dedication along with 
; this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
; 
; ******************************************************************************


%define NUCOG_ITEM_(n, t, c, v) c equ v
%define NUCOG_ITEM(n, t, c, v) NUCOG_ITEM_(n, t, c, v),

%macro NUCOG_GROUP_ 1-*
%rep %0
%1
%rotate 1
%endrep
%endmacro

%define NUCOG_GROUP(group, item_name, items) NUCOG_GROUP_ items

