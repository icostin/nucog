/******************************************************************************

nucog - header-only library for working with type-safe numeric constant groups.

Written in 2019 by Costin Ionescu <costin.ionescu@gmail.com>


To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

******************************************************************************/

#ifndef _NUCOG_H
#define _NUCOG_H

#include <stdint.h>

#ifndef NUCOG_BOOL
#include <stdbool.h>
#define NUCOG_BOOL bool
#endif

#ifndef NUCOG_FALSE
# define NUCOG_FALSE false
#endif

#ifndef NUCOG_TRUE
# define NUCOG_TRUE true
#endif

#ifndef NUCOG_INLINE
#define NUCOG_INLINE static inline
#endif

#ifndef NUCOG_RESTRICT
# if defined(__cplusplus)
#  define NUCOG_RESTRICT
# else
#  define NUCOG_RESTRICT restrict
# endif
#endif

#define NUCOG_TYPE_u8 uint8_t
#define NUCOG_TYPE_i8 int8_t
#define NUCOG_TYPE_u16 uint16_t
#define NUCOG_TYPE_i16 int16_t
#define NUCOG_TYPE_u32 uint32_t
#define NUCOG_TYPE_i32 int32_t
#define NUCOG_TYPE_u64 uint64_t
#define NUCOG_TYPE_i64 int64_t

#define NUCOG_ITEM(g, p, c, v) (g, p, c, v)

#define NUCOG_TP1(a, b) a ## b
#define NUCOG_TP2(a, b) NUCOG_TP1(a, b)
#define NUCOG_TP_ABC(a, b, c) a ## b ## c

#define NUCOG_EVAL1(...) __VA_ARGS__
#define NUCOG_EVAL2(...) NUCOG_EVAL1(NUCOG_EVAL1(__VA_ARGS__))
#define NUCOG_EVAL4(...) NUCOG_EVAL2(NUCOG_EVAL2(__VA_ARGS__))
#define NUCOG_EVAL8(...) NUCOG_EVAL4(NUCOG_EVAL4(__VA_ARGS__))
#define NUCOG_EVAL16(...) NUCOG_EVAL8(NUCOG_EVAL8(__VA_ARGS__))
#define NUCOG_EVAL32(...) NUCOG_EVAL16(NUCOG_EVAL16(__VA_ARGS__))
#define NUCOG_EVAL64(...) NUCOG_EVAL32(NUCOG_EVAL32(__VA_ARGS__))
#define NUCOG_EVAL128(...) NUCOG_EVAL64(NUCOG_EVAL64(__VA_ARGS__))
#define NUCOG_EVAL256(...) NUCOG_EVAL128(NUCOG_EVAL128(__VA_ARGS__))
#define NUCOG_EVAL512(...) NUCOG_EVAL256(NUCOG_EVAL256(__VA_ARGS__))
#define NUCOG_EVAL1024(...) NUCOG_EVAL512(NUCOG_EVAL512(__VA_ARGS__))
#define NUCOG_EVAL NUCOG_EVAL1024

#define NUCOG_SWITCH(expr) switch ((expr).n_)
#define NUCOG_VALUE(ct) ((ct##_value_t) ((uint64_t) ct##__LO32 | ((uint64_t) ct##__HI32 << 32)))
#define NUCOG_CASE(ct) case NUCOG_VALUE(ct)

#define NUCOG_ITEMSTMT0(g, p, c, v) \
    static g##_t const c = { (v) }; \
    typedef NUCOG_TYPE_##p c##_value_t; \
    enum c##_enum_lo32 { c##__LO32 = (uint32_t) ((uint64_t) (v) & 0xFFFFFFFF) }; \
    enum c##_enum_hi32 { c##__HI32 = (uint32_t) ((uint64_t) (v) >> 32) }; \
    NUCOG_INLINE NUCOG_BOOL IS_##c (g##_t x) { return g##_equ(x, c); }
#define NUCOG_ITEMSTMT1(g, p, c, v) NUCOG_ITEMSTMT0(g,p,c,v) NUCOG_ITEMSTMT2
#define NUCOG_ITEMSTMT2(g, p, c, v) NUCOG_ITEMSTMT0(g,p,c,v) NUCOG_ITEMSTMT1
#define NUCOG_ITEMSTMT1_
#define NUCOG_ITEMSTMT2_

#define NUCOG_ITEMCASE0(g, p, c, v) case v: *cp = c; return NUCOG_TRUE;
#define NUCOG_ITEMCASE1(g, p, c, v) NUCOG_ITEMCASE0(g,p,c,v) NUCOG_ITEMCASE2
#define NUCOG_ITEMCASE2(g, p, c, v) NUCOG_ITEMCASE0(g,p,c,v) NUCOG_ITEMCASE1
#define NUCOG_ITEMCASE1_
#define NUCOG_ITEMCASE2_

#define NUCOG_NAMECASE0(g, p, c, v) case v: return #c;
#define NUCOG_NAMECASE1(g, p, c, v) NUCOG_NAMECASE0(g,p,c,v) NUCOG_NAMECASE2
#define NUCOG_NAMECASE2(g, p, c, v) NUCOG_NAMECASE0(g,p,c,v) NUCOG_NAMECASE1
#define NUCOG_NAMECASE1_
#define NUCOG_NAMECASE2_

#define NUCOG_PLUSONE0(g, p, c, v) + 1
#define NUCOG_PLUSONE1(g, p, c, v) NUCOG_PLUSONE0(g,p,c,v) NUCOG_PLUSONE2
#define NUCOG_PLUSONE2(g, p, c, v) NUCOG_PLUSONE0(g,p,c,v) NUCOG_PLUSONE1
#define NUCOG_PLUSONE1_
#define NUCOG_PLUSONE2_

#define NUCOG_TINIT0(g, p, c, v) names[i] = #c; arr[i++] = c;
#define NUCOG_TINIT1(g, p, c, v) NUCOG_TINIT0(g,p,c,v) NUCOG_TINIT2
#define NUCOG_TINIT2(g, p, c, v) NUCOG_TINIT0(g,p,c,v) NUCOG_TINIT1
#define NUCOG_TINIT1_
#define NUCOG_TINIT2_

#define NUCOG_TCHK0(g, p, c, v) passed &= IS_##c(arr[i]) == (arr[i].n_ == v);
#define NUCOG_TCHK1(g, p, c, v) NUCOG_TCHK0(g,p,c,v) NUCOG_TCHK2
#define NUCOG_TCHK2(g, p, c, v) NUCOG_TCHK0(g,p,c,v) NUCOG_TCHK1
#define NUCOG_TCHK1_
#define NUCOG_TCHK2_

#define NUCOG_GROUP(group, pmv, items) \
    typedef struct group##_s { NUCOG_TYPE_##pmv n_; } group##_t; \
    NUCOG_INLINE NUCOG_BOOL group##_equ (group##_t cta, group##_t ctb) { \
        return cta.n_ == ctb.n_; } \
    NUCOG_EVAL(NUCOG_TP2(NUCOG_ITEMSTMT1 items,_)) \
    NUCOG_INLINE NUCOG_BOOL group##_convert_##pmv (\
                group##_t * NUCOG_RESTRICT cp, NUCOG_TYPE_##pmv n) { \
        switch (n) { NUCOG_EVAL(NUCOG_TP2(NUCOG_ITEMCASE1 items,_)) \
        default: return NUCOG_FALSE; }} \
    NUCOG_INLINE group##_t group##_from_##pmv (NUCOG_TYPE_##pmv n, \
                                               group##_t no_match_value) { \
        group##_t x; \
        return group##_convert_##pmv(&x, n) ? x : no_match_value; } \
    NUCOG_INLINE NUCOG_TYPE_##pmv group##_to_##pmv (group##_t ct) { \
        return ct.n_; } \
    NUCOG_INLINE char const * group##_name (group##_t ct) { \
        switch (ct.n_) { NUCOG_EVAL(NUCOG_TP2(NUCOG_NAMECASE1 items,_)) \
        default: return "_BAD_CONST(" #group ")"; }} \
    NUCOG_INLINE int group##_self_test (void) { \
        unsigned long const n = 0 \
            NUCOG_EVAL(NUCOG_TP2(NUCOG_PLUSONE1 items,_)); \
        group##_t arr[n], ct; \
        char const * names[n]; \
        char const * name; \
        char const * bad_const_name = "_BAD_CONST(" #group ")"; \
        unsigned long i = 0, j; \
        unsigned int passed = 1; \
        NUCOG_TYPE_##pmv out_of_group = 0; \
        NUCOG_EVAL(NUCOG_TP2(NUCOG_TINIT1 items,_)) \
        do { out_of_group++; \
            for (i = 0; i < n && arr[i].n_ != out_of_group; ++i); \
        } while (i < n); \
        for (i = 0; i < n; ++i) { \
            for (j = 0; j < n; ++j) { \
                passed &= group##_equ(arr[i], arr[j]) \
                    == (arr[i].n_ == arr[j].n_); \
            } \
            NUCOG_EVAL(NUCOG_TP2(NUCOG_TCHK1 items,_)) \
            passed &= group##_convert_##pmv(&ct, arr[i].n_); \
            passed &= (ct.n_ == arr[i].n_); \
            passed &= group##_equ( \
                group##_from_##pmv(arr[i].n_, arr[(i + 1) % n]), arr[i]); \
            passed &= group##_equ( \
                group##_from_##pmv(out_of_group, arr[i]), arr[i]); \
            passed &= (group##_to_##pmv(arr[i]) == arr[i].n_); \
            name = group##_name(arr[i]); \
            for (j = 0; names[i][j] == name[j] && names[i][j]; ++j); \
            passed &= (name[j] == 0); \
        } \
        passed &= !group##_convert_##pmv(&ct, out_of_group); \
        ct.n_ = out_of_group; \
        name = group##_name(ct); \
        for (j = 0; bad_const_name[j] == name[j] && name[j]; ++j); \
        passed &= (name[j] == 0); \
        return passed; \
    }

#endif /* _NUCOG_H */
