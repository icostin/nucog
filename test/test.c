#include <stdio.h>
#include <stdlib.h>
#include "nucog.h"
#include "test_bla.inc"
#include "test_fbz.inc"

int cov_test (void);

#define T(expr) if ((expr)) ; else return (fprintf(stderr, "%s:%u: expression is false: %s\n", __FILE__, __LINE__, #expr), 1)
int main (int argc, char const * const * argv)
{
    int i;

    T(NUCOG_TRUE != NUCOG_FALSE);
    T(cov_test());

    T(bla_self_test());
    T(fbz_self_test());

    T(IS_BLA_OK(BLA_OK));
    T(!IS_BLA_OK(BLA_ABC));
    T(!IS_BLA_OK(BLA_DEF));
    T(!IS_BLA_OK(BLA_GHI));

    T(!IS_BLA_ABC(BLA_OK));
    T(IS_BLA_ABC(BLA_ABC));
    T(!IS_BLA_ABC(BLA_DEF));
    T(!IS_BLA_ABC(BLA_GHI));

    T(!IS_BLA_DEF(BLA_OK));
    T(!IS_BLA_DEF(BLA_ABC));
    T(IS_BLA_DEF(BLA_DEF));
    T(!IS_BLA_DEF(BLA_GHI));

    T(!IS_BLA_GHI(BLA_OK));
    T(!IS_BLA_GHI(BLA_ABC));
    T(!IS_BLA_GHI(BLA_DEF));
    T(IS_BLA_GHI(BLA_GHI));

    T(IS_FBZ_NONE(FBZ_NONE));
    T(!IS_FBZ_NONE(FBZ_FOO));
    T(!IS_FBZ_NONE(FBZ_BAR));
    T(!IS_FBZ_NONE(FBZ_BAZ));
    T(!IS_FBZ_NONE(FBZ_EVIL));

    T(!IS_FBZ_FOO(FBZ_NONE));
    T(IS_FBZ_FOO(FBZ_FOO));
    T(!IS_FBZ_FOO(FBZ_BAR));
    T(!IS_FBZ_FOO(FBZ_BAZ));
    T(!IS_FBZ_FOO(FBZ_EVIL));

    T(!IS_FBZ_BAR(FBZ_NONE));
    T(!IS_FBZ_BAR(FBZ_FOO));
    T(IS_FBZ_BAR(FBZ_BAR));
    T(!IS_FBZ_BAR(FBZ_BAZ));
    T(!IS_FBZ_BAR(FBZ_EVIL));

    T(!IS_FBZ_BAZ(FBZ_NONE));
    T(!IS_FBZ_BAZ(FBZ_FOO));
    T(!IS_FBZ_BAZ(FBZ_BAR));
    T(IS_FBZ_BAZ(FBZ_BAZ));
    T(!IS_FBZ_BAZ(FBZ_EVIL));

    T(!IS_FBZ_EVIL(FBZ_NONE));
    T(!IS_FBZ_EVIL(FBZ_FOO));
    T(!IS_FBZ_EVIL(FBZ_BAR));
    T(!IS_FBZ_EVIL(FBZ_BAZ));
    T(IS_FBZ_EVIL(FBZ_EVIL));



    for (i = 1; i < argc; ++i)
    {
        bla_t b;
        fbz_t f;
        int n;

        if (bla_convert_u32(&b, (uint32_t) atoi(argv[i])))
            printf("%s -> %s = %u%s\n", argv[i], bla_name(b), bla_to_u32(b),
                   bla_equ(b, BLA_OK) ? "!!!" : "");
        else if (fbz_convert_u64(&f, (uint32_t) atoi(argv[i])))
            printf("%s -> %s = %llu%s\n", argv[i], fbz_name(f), 
                   (unsigned long long) fbz_to_u64(f),
                   fbz_equ(f, FBZ_EVIL) ? " >:-E" : "");
        else printf("%s is not bla nor fbz\n", argv[i]);
        n = 0;
        switch (atoi(argv[i])) {
        NUCOG_CASE(FBZ_FOO): n = 1; break;
        NUCOG_CASE(FBZ_BAR): n = 2; break;
        NUCOG_CASE(FBZ_EVIL): n = 3; break;
        }
        printf("n=%d\n", n);
    }

    return 0;
}

