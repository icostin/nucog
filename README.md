# nucog

Header-only library for working with type-safe numeric constant groups.

This library source is released into the public domain,
through CC0 (Common Creative Zero).

