.PHONY: test asmtest ctest cview clean llcov llcov-show install

PREFIX_DIR := $(HOME)/.local
YASM := yasm
t := _tmp
LLVM_PROFDATA=llvm-profdata
LLVM_COV=llvm-cov

test: asmtest ctest

clean:
	-rm -rf $t/

asmtest: $t/test.bin
	md5sum $< | grep 79eba49b50b937425d3e2de6e18d10f4
	@echo asm test passed

ctest: $t/ctest
	$t/ctest 0 111 123 222 333 444 456 555 666 789
	
llcov: $t/ctest.profdata
	$(LLVM_COV) report $t/ctest-llcov -instr-profile=$t/ctest.profdata

llcov-show: $t/ctest.profdata
	$(LLVM_COV) show $t/ctest-llcov -instr-profile=$t/ctest.profdata

$t/ctest.profdata: $t/ctest.profraw
	$(LLVM_PROFDATA) merge -sparse $< -o $@

$t/ctest.profraw: $t/ctest-llcov
	LLVM_PROFILE_FILE=$@ $<

%/:
	mkdir -p $@

$t/test.bin: test/test.asm lib-src/nucog.inc test/test_bla.inc test/test_fbz.inc | $t/
	$(YASM) -o$@ $< -I lib-src

$t/ctest: test/test.c test/cov.c lib-src/nucog.h test/test_bla.inc test/test_fbz.inc | $t/
	$(CC) -o$@ $(filter %.c,$^) -I lib-src

$t/ctest-llcov: $t/cov-test.c.o $t/cov-cov.c.o
	$(CC) -fprofile-instr-generate -fcoverage-mapping -o$@ $^

$t/cov-%.o: test/% lib-src/nucog.h test/test_bla.inc test/test_fbz.inc | $t/
	$(CC) -c $(and $(findstring cov,$<),-fprofile-instr-generate -fcoverage-mapping) -I lib-src -o $@ $<

cview:
	$(CC) -E test.c

install:
	mkdir -p $(PREFIX_DIR)/include $(PREFIX_DIR)/include/yasm
	install -t $(PREFIX_DIR)/include -m 444 lib-src/nucog.h
	install -t $(PREFIX_DIR)/include/yasm -m 444 lib-src/nucog.inc
